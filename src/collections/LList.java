package collections;

public class LList<E> implements IList<E> {

    private Node<E> head;
    private Node<E> end;
    private int size;

    private static class Node<E> {
        E val;
        Node<E> next;
        Node<E> prev;

        public Node(E val) {
            this.val = val;
            next = null;
            prev = null;
        }
    }


    public LList() {
    }

    public LList(E[] array) {
        for (E element : array) {
            add(element);
        }
    }

    @Override
    public void clear() {
        head = null;
        end = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> result = head;
        for (int i = 0; i < index; i++) {
            result = result.next;
        }

        return result.val;
    }

    @Override
    public boolean add(E number) {
        return addLast(number);
    }

    @Override
    public boolean add(int index, E number) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node newNode = new Node(number);
        if (index == 0) {
            addStart(number);
        } else {
            Node previous = head;
            int count = 0;
            while (count < index - 1) {
                previous = previous.next;
                count++;
            }
            Node current = previous.next;
            newNode.next = current;
            previous.next = newNode;

        }
        size++;
        return true;
    }

    public boolean addStart(E value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;

        } else {
            newNode.next = head;
            head = newNode;
        }
        size++;
        return true;
    }

    public boolean addLast(E value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (null != current.next) {
                current = current.next;
            }
            current.next = newNode;
        }
        size++;
        return true;
    }

    @Override
    public E remove(E number) {
        Node current = head;
        Node temp = null;
        if (current != null && current.val.equals(number)) {
            head = current.next;
            return number;
        }
        while (current != null && !current.val.equals(number)) {
            temp = current;
            current = current.next;
        }
        if (current == null) {
            return null;
        }
        temp.next = current.next;
        size--;
        return number;
    }

    @Override
    public E removeByIndex(int index) {
        if (index < 0 || index > size - 1) {
            throw new IllegalArgumentException();
        }
        if (index == 0) {
            Node temp = head;
            head = head.next;
            temp.next = null;
        } else {
            Node previous = head;
            int count = 0;
            while (count < index - 1) {
                previous = previous.next;
                count++;
            }
            Node current = previous.next;
            previous.next = current.next;
            current.next = null;
        }
        size--;
        return (E) head;
    }

    @Override
    public boolean contains(E number) {
        for (int i = 0; i < size; i++) {
            if (get(i).equals(number)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        if (head == null) {
            System.out.print("[]");
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Node current = head;
            while (current != null) {
                stringBuilder.append(current.val + ", ");
                current = current.next;
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            System.out.println(stringBuilder);
        }

    }

    @Override
    public E[] toArray() {
        Object[] array = new Object[0];
        if (head == null) {
        } else {
            Node current = head;
            array = new Object[size];
            int count = 0;
            while (current != null) {
                array[count] = current.val;
                current = current.next;
                count++;
            }
        }
        return (E[]) array;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Node current = head;
            while (current != null) {
                stringBuilder.append(current.val + ", ");
                current = current.next;
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            return stringBuilder.toString();
        }
    }

    @Override
    public IList<E> subList(int fromIdex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(E[] arr) {
        return false;
    }

    @Override
    public boolean retainAll(E[] arr) {
        return false;
    }
}
