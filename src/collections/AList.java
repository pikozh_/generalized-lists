package collections;

import java.util.Arrays;

public class AList<E> implements IList<E> {

    private static final int CAPACITY = 10;

    private Object[] arrayOfAlist;

    private static final Object[] EMPTY_LIST = {};

    private int size;

    public AList() {
        this.arrayOfAlist = new Object[CAPACITY];
    }

    public AList(int capacity) {
        if (capacity > 0) {
            this.arrayOfAlist = new Object[capacity];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + capacity);
        }
    }

    public AList(E[] array) {
        this.arrayOfAlist = new Object[array.length];
        for (int i = 0; i < array.length; i++) {
            this.arrayOfAlist[i] = array[i];
            this.size++;
        }
    }

    @Override
    public void clear() {
        this.arrayOfAlist = EMPTY_LIST;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index <= this.size) {
            return (E) arrayOfAlist[index];
        } else {
            throw new IllegalArgumentException("Illegal index: " + index);
        }
    }

    @Override
    public boolean add(E number) {
        Object[] temp = Arrays.copyOf(arrayOfAlist, (int) ((this.arrayOfAlist.length + 1) * 1.2));
        temp[size] = number;
        this.arrayOfAlist = temp;
        this.size++;
        return true;
    }

    @Override
    public boolean add(int index, E number) {
        if (index >= 0 && index < this.size) {

            Object[] temp = new Object[(int) ((this.arrayOfAlist.length + 1) * 1.2)];
            System.arraycopy(this.arrayOfAlist, 0, temp, 0, index);
            temp[index] = number;
            System.arraycopy(this.arrayOfAlist, index, temp, index + 1, this.size - index);

            this.arrayOfAlist = temp;
            this.size++;
            return true;
        } else {
            throw new IndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    @Override
    public E remove(E number) {
        int searchableIndex = -1;
        for (int i = 0; i < size; i++) {
            if (this.arrayOfAlist[i] == number) {
                searchableIndex = i;
                break;
            }
        }
        if (searchableIndex != -1) {
            System.arraycopy(this.arrayOfAlist, searchableIndex + 1, this.arrayOfAlist, searchableIndex, this.arrayOfAlist.length - searchableIndex - 1);
            this.size--;
            return number;
        } else {
            return null;
        }
    }

    @Override
    public E removeByIndex(int index) {
        if (index >= 0 && index < this.size) {
            System.arraycopy(this.arrayOfAlist, index + 1, this.arrayOfAlist, index, this.arrayOfAlist.length - index - 1);
            this.size--;
            return (E) arrayOfAlist[index];
        } else {
            return null;
        }
    }

    @Override
    public boolean contains(E number) {
        for (int i = 0; i < size; i++) {
            if (this.arrayOfAlist[i].equals(number)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        if (size == 0) {
            System.out.println("[]");
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            for (int i = 0; i < size; i++) {
                stringBuilder.append(arrayOfAlist[i] + ", ");
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            System.out.println(stringBuilder);
        }

    }

    @Override
    public E[] toArray() {
        Object[] result = new Object[arrayOfAlist.length];
        for (int i = 0; i < size; i++) {
            result[i] = arrayOfAlist[i];
        }
        return (E[]) result;
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        AList list = new AList();
        for (int i = fromIdex; i < toIndex; i++) {
            list.add(this.arrayOfAlist[i]);
        }
        return list;
    }

    @Override
    public boolean removeAll(E[] arr) {
        for (int i = 0; i < this.size; i++) {
            for (E a : arr) {
                this.remove(a);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(E[] arr) {
        E[] temp;
        if (arr.length > 10) {
            temp = (E[]) new Object[arr.length];
        } else {
            temp = (E[]) new Object[10];
        }

        int index = 0;
        for (int i = 0; i < this.size; i++) {
            for (E a : arr) {
                if (a == this.arrayOfAlist[i]) {
                    temp[index] = a;
                    index++;
                }
            }
        }
        this.arrayOfAlist = temp;
        return true;
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            for (int i = 0; i < size; i++) {
                stringBuilder.append(arrayOfAlist[i] + ", ");
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            return stringBuilder.toString();
        }
    }
}
