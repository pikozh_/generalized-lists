import collections.AList;
import collections.LList;

public class Main {
    public static void main(String[] args) {
        AList aList = new AList();
        aList.add("Хочу");
        aList.add("в");
        aList.add("WizardsDev");
        aList.print();

        LList lList = new LList();
        lList.add("Хочу");
        lList.add("в");
        lList.add("WizardsDev");
        lList.print();
    }
}
