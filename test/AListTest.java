import collections.AList;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;


public class AListTest {


    @Test
    public void clearPass() {
        //given
        AList<Integer> aList = new AList<>();
        aList.add(1);
        aList.add(2);

        //when
        aList.clear();
        String expected = "[]";
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeShouldPass() {
        //given
        AList<Integer> aList = new AList<Integer>();
        aList.add(1);
        aList.add(2);

        //when
        int expected = 2;
        int actual = aList.size();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getShouldPass() {
        //given
        AList<Integer> aList = new AList<Integer>();
        aList.add(1);
        aList.add(2);

        //when
        int expected = 2;
        int actual = aList.get(1);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addShouldPassTrue() {
        //given
        AList aList = new AList();

        //when
        boolean actual = aList.add(1);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void addShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<Integer>(array);

        //when
        int[] arrayForExpected = {1, 2, 3, 4, 5};
        String expected = Arrays.toString(arrayForExpected);
        aList.add(5);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addWithIndexShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<Integer>(array);

        //when
        int[] arrayForExpected = {1, 2, 10, 3, 4};
        String expected = Arrays.toString(arrayForExpected);
        aList.add(2, 10);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<>(array);

        //when
        int[] arrayForExpected = {1, 2, 4};
        String expected = Arrays.toString(arrayForExpected);
        aList.remove(3);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeByIndexShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<>(array);

        //when
        int[] arrayForExpected = {1, 2, 4};
        String expected = Arrays.toString(arrayForExpected);
        aList.removeByIndex(2);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void containsShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<>(array);

        //when
        boolean actual = aList.contains(4);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void printShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4};
        AList<Integer> aList = new AList<>(array);

        //when
        int[] arrayForExpected = {1, 2, 3, 4};
        String expected = Arrays.toString(arrayForExpected);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sublistShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4, 5, 6};
        AList<Integer> aList = new AList<>(array);

        //when
        Integer[] arrayForExpected = {2, 3, 4};
        String expected = Arrays.toString(arrayForExpected);
        String actual = aList.subList(1, 4).toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeAllShouldPass() {
        //given
        Integer[] array = {1, 2, 3, 4, 5, 6};
        AList aList = new AList(array);

        //when
        Integer[] arrayForRemove = {7, 2, 5};
        int[] arrayForExpected = {1, 3, 4, 6};
        String expected = Arrays.toString(arrayForExpected);
        aList.removeAll(arrayForRemove);
        String actual = aList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }
}

